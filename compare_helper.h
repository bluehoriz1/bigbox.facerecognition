//
// Created by mireo on 03.05.2021.
//
#pragma once
#include <iostream>
#include <librealsense2/rs.hpp>   // Include RealSense Cross Platform API
#include <dlib/image_processing/frontal_face_detector.h>
#include "dlib/opencv/cv_image.h"

#include "rs2/opencv/cv-helpers.hpp"

#include <dlib/dnn.h>

namespace fr {
    template<template<int, template<typename> class, int, typename> class block, int N,
            template<typename> class BN, typename SUBNET>
    using residual = dlib::add_prev1<block<N, BN, 1, dlib::tag1<SUBNET>>>;

    template<template<int, template<typename> class, int, typename> class block, int N,
            template<typename> class BN, typename SUBNET>
    using residual_down = dlib::add_prev2<dlib::avg_pool<2, 2, 2, 2, dlib::skip1<dlib::tag2<block<N, BN, 2, dlib::tag1<SUBNET>>>>>>;

    template<int N, template<typename> class BN, int stride, typename SUBNET>
    using block = BN<dlib::con<N, 3, 3, 1, 1, dlib::relu<BN<dlib::con<N, 3, 3, stride, stride, SUBNET>>>>>;

    template<int N, typename SUBNET> using ares = dlib::relu<residual<block, N, dlib::affine, SUBNET>>;
    template<int N, typename SUBNET> using ares_down = dlib::relu<residual_down<block, N, dlib::affine, SUBNET>>;
    template<typename SUBNET> using alevel0 = ares_down<256, SUBNET>;
    template<typename SUBNET> using alevel1 = ares<256, ares<256, ares_down<256, SUBNET>>>;
    template<typename SUBNET> using alevel2 = ares<128, ares<128, ares_down<128, SUBNET>>>;
    template<typename SUBNET> using alevel3 = ares<64, ares<64, ares<64, ares_down<64, SUBNET>>>>;
    template<typename SUBNET> using alevel4 = ares<32, ares<32, ares<32, SUBNET>>>;
    using anet_type = dlib::loss_metric<dlib::fc_no_bias<128, dlib::avg_pool_everything<
            alevel0<
                    alevel1<
                            alevel2<
                                    alevel3<
                                            alevel4<
                                                    dlib::max_pool<3, 3, 2, 2, dlib::relu<dlib::affine<dlib::con<32, 7, 7, 2, 2,
                                                            dlib::input_rgb_image_sized<150>
                                                    >>>>>>>>>>>>;

    std::vector<dlib::matrix<dlib::rgb_pixel>> jitter_image(
            const dlib::matrix<dlib::rgb_pixel> &img
    );

    static void render_face_element(cv::Mat &img,
                                    dlib::full_object_detection const &face,
                                    const cv::Scalar &color,
                                    const unsigned long &from, const unsigned long &to) {
        dlib::point point1 = face.part(from);
        dlib::point point2 = face.part(to);
        cv::line(img,
                 cv::Point((int) point1.x(), (int) point1.y()),
                 cv::Point((int) point2.x(), (int) point2.y()),
                 color,
                 1,
                 cv::LINE_8);
    }

    static dlib::matrix<dlib::rgb_pixel>
    get_face_chip(const dlib::matrix<dlib::rgb_pixel> &image, const dlib::full_object_detection &shape) {
        dlib::matrix<dlib::rgb_pixel> face_chip;
        extract_image_chip(image, get_face_chip_details(shape, 150, 0.25), face_chip);
//    test_faces.push_back(std::move(face_chip));
        return face_chip;
    }

    static float get_depth_scale(const rs2::device &dev) {
        // Go over the device's sensors
        for (rs2::sensor &sensor : dev.query_sensors()) {
            // Check if the sensor if a depth sensor
            if (rs2::depth_sensor dpt = sensor.as<rs2::depth_sensor>()) {
                return dpt.get_depth_scale();
            }
        }
        throw std::runtime_error("Device does not have a depth sensor");
    }

    static void render_landmarks(cv::Mat &img,
                                 dlib::full_object_detection const &face,
                                 dlib::rgb_pixel const &color
    ) {

        cv::Scalar col(color.blue, color.green, color.red);


        // Around Chin. Ear to Ear
        for (unsigned long i = markup_68::JAW_FROM; i < markup_68::JAW_TO; ++i)
            render_face_element(img, face, col, i, i + 1);

        // Nose
        for (unsigned long i = markup_68::NOSE_RIDGE_FROM; i < markup_68::NOSE_RIDGE_TO; ++i)
            render_face_element(img, face, col, i, i + 1);
        render_face_element(img, face, col, markup_68::NOSE_TIP, markup_68::NOSE_BOTTOM_FROM);
        render_face_element(img, face, col, markup_68::NOSE_TIP, markup_68::NOSE_BOTTOM_TO);

        // Left eyebrow
        for (unsigned long i = markup_68::RIGHT_EYEBROW_FROM; i < markup_68::RIGHT_EYEBROW_TO; ++i)
            render_face_element(img, face, col, i, i + 1);

        // Right eyebrow
        for (unsigned long i = markup_68::LEFT_EYEBROW_FROM; i < markup_68::LEFT_EYEBROW_TO; ++i)
            render_face_element(img, face, col, i, i + 1);

        // Right eye
        for (unsigned long i = markup_68::RIGHT_EYE_FROM; i < markup_68::RIGHT_EYE_TO; ++i)
            render_face_element(img, face, col, i, i + 1);
        render_face_element(img, face, col, markup_68::RIGHT_EYE_FROM, markup_68::RIGHT_EYE_TO);

        // Left eye
        for (unsigned long i = markup_68::LEFT_EYE_FROM; i < markup_68::LEFT_EYE_TO; ++i)
            render_face_element(img, face, col, i, i + 1);
        render_face_element(img, face, col, markup_68::LEFT_EYE_FROM, markup_68::LEFT_EYE_TO);

        // Lips inside part
        for (unsigned long i = markup_68::MOUTH_INNER_FROM; i < markup_68::MOUTH_INNER_TO; ++i)
            render_face_element(img, face, col, i, i + 1);
        render_face_element(img, face, col, markup_68::MOUTH_INNER_FROM, markup_68::MOUTH_INNER_TO);
    }

    static cv::Mat render_face_view(const dlib::full_object_detection &face, const rs2::video_frame &color_frame,
                                    const dlib::rgb_pixel &color, const int &offset = 25) {
//    int offset = 25;
        cv::Mat img = frame_to_mat(color_frame);
        render_landmarks(img, face, color);
        int left = (int) face.get_rect().left() - offset > 0 ? (int) face.get_rect().left() - offset : 0;
        int top = (int) face.get_rect().top() - offset > 0 ? (int) face.get_rect().top() - offset : 0;
        int right = (int) face.get_rect().right() + offset < color_frame.get_width() ? (int) face.get_rect().right() +
                                                                                       offset :
                    color_frame.get_width() - 1;
        int bottom =
                (int) face.get_rect().bottom() + offset < color_frame.get_height() ? (int) face.get_rect().bottom() +
                                                                                     offset : color_frame.get_height() -
                                                                                              1;
        return img(cv::Rect(cv::Point(left, top), cv::Point(right, bottom)));
    }

//int compare_images(){
//    compare_images
//}


    static dlib::matrix<dlib::rgb_pixel> mat_to_rgbpixel(const cv::Mat &image) {
        dlib::cv_image<dlib::bgr_pixel> temp(image);
        dlib::matrix<dlib::rgb_pixel> spimg;
        dlib::assign_image(spimg, temp);
        return spimg;
    }

    static dlib::matrix<dlib::rgb_pixel> uchar_to_rgbpixel(unsigned char *image, int length) {
        std::vector<uchar> data = std::vector<uchar>(image, image + length);
        cv::Mat ImMat = cv::imdecode(data, 0);
        return mat_to_rgbpixel(ImMat);
    }
}