//
// Created by mireo on 30.04.2021.
//

#ifndef REALSENSEDLIBSAMPLES_MYCLASS_H
#define REALSENSEDLIBSAMPLES_MYCLASS_H
#include <iostream>
#include <librealsense2/rs.hpp>   // Include RealSense Cross Platform API
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>     // image_window, etc.
#include "../rs_frame_image.h"
#include "validate_face.h"
#include "render_face.h"
#include <opencv4/opencv2/highgui.hpp>
#include "req_res.h"

#include "server.h"
#include <future>
#include "../../opencv/cv-helpers.hpp"

#include <dlib/dnn.h>
#include <dlib/gui_widgets.h>
#include <dlib/clustering.h>
#include <dlib/string.h>
#include <dlib/image_io.h>
#include <dlib/image_processing/frontal_face_detector.h>

template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual = dlib::add_prev1<block<N,BN,1,dlib::tag1<SUBNET>>>;

template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual_down = dlib::add_prev2<dlib::avg_pool<2,2,2,2,dlib::skip1<dlib::tag2<block<N,BN,2,dlib::tag1<SUBNET>>>>>>;

template <int N, template <typename> class BN, int stride, typename SUBNET>
using block  = BN<dlib::con<N,3,3,1,1,dlib::relu<BN<dlib::con<N,3,3,stride,stride,SUBNET>>>>>;

template <int N, typename SUBNET> using ares      = dlib::relu<residual<block,N,dlib::affine,SUBNET>>;
template <int N, typename SUBNET> using ares_down = dlib::relu<residual_down<block,N,dlib::affine,SUBNET>>;
template <typename SUBNET> using alevel0 = ares_down<256,SUBNET>;
template <typename SUBNET> using alevel1 = ares<256,ares<256,ares_down<256,SUBNET>>>;
template <typename SUBNET> using alevel2 = ares<128,ares<128,ares_down<128,SUBNET>>>;
template <typename SUBNET> using alevel3 = ares<64,ares<64,ares<64,ares_down<64,SUBNET>>>>;
template <typename SUBNET> using alevel4 = ares<32,ares<32,ares<32,SUBNET>>>;
using anet_type = dlib::loss_metric<dlib::fc_no_bias<128,dlib::avg_pool_everything<
                                             alevel0<
                                             alevel1<
                                             alevel2<
                                             alevel3<
                                             alevel4<
                                             dlib::max_pool<3,3,2,2,dlib::relu<dlib::affine<dlib::con<32,7,7,2,2,
                                             dlib::input_rgb_image_sized<150>
>>>>>>>>>>>>;

std::vector<dlib::matrix<dlib::rgb_pixel>> jitter_image(
        const dlib::matrix<dlib::rgb_pixel>& img
);

static void render_face_element(cv::Mat &img,
                                dlib::full_object_detection const & face,
                                cv::Scalar & color,
                                unsigned long from, unsigned long to){
    dlib::point point1 = face.part(from);
    dlib::point point2 = face.part(to);
    cv::line(img,
             cv::Point(point1.x(), point1.y()),
             cv::Point(point2.x(), point2.y()),
             color,
             1,
             cv::LINE_8 );
}

class MyClass {
    public:
        dlib::rgb_pixel bad_color;
        dlib::rgb_pixel good_color;

        dlib::matrix<dlib::rgb_pixel> compare_img;

        dlib::matrix<float,0,1> id_descriptor;

        MyClass(uint16_t p = 5555) {     // Constructor
//            port = p;
            rs2::config cfg;

            //Add desired streams to configuration
            cfg.enable_stream(RS2_STREAM_COLOR, 640, 480, RS2_FORMAT_RGB8, 15);
            cfg.enable_stream(RS2_STREAM_DEPTH, 640, 480, RS2_FORMAT_Z16, 15);
//            cfg.enable_stream(RS2_STREAM_DEPTH, 640, 480, RS2_FORMAT_RGB8, 15);
//            profile = pipe.start(cfg);

            s = new http::Server(p);
//            uint16_t port = 5555;                   /// Set the port
//            http::Server s(port);
//            cv::VideoCapture v(0, cv::CAP_V4L2);    /// Set the correct device id
//            std::mutex mtx;
//            auto fut = std::async ([&]{is_prime();});
            good_color.red = 0;
            good_color.green = 255;
            good_color.blue = 0;
            bad_color.red = 255;
            bad_color.green = 0;
            bad_color.blue = 0;
//            good_color = new dlib::rgb_pixel( 0, 255, 0 );
//            bad_color *= new dlib::rgb_pixel( 255, 0, 0 );

            dlib::deserialize( "shape_predictor_68_face_landmarks.dat" ) >> face_landmark_annotator;
            // And finally we load the DNN responsible for face recognition.
            dlib::deserialize("dlib_face_recognition_resnet_model_v1.dat") >> net;

            rs2::align align_to_color( RS2_STREAM_COLOR );

            dlib::load_image(compare_img, "mczernik.jpg");
           dlib::matrix<dlib::rgb_pixel> test_faces;
            for (auto face : face_bbox_detector(compare_img))
            {
                auto shape = face_landmark_annotator(compare_img, face);
                dlib::matrix<dlib::rgb_pixel> face_chip;
                extract_image_chip(compare_img, get_face_chip_details(shape,150,0.25), face_chip);
//                test_faces.push_back(std::move(face_chip));
                test_faces = face_chip;
            }

            dlib::matrix<float,0,1> face_descriptors = net(test_faces);

            id_descriptor = face_descriptors;
//            std::cout << length(face_descriptors[0]) << std::endl;


            s->get("/video_feed", [&] (Request req, Response res){
                profile = pipe.start(cfg);

                DEVICE_DEPTH_SCALE = get_depth_scale( profile.get_device() );
                res.headers.push_back("Connection: close");
                res.headers.push_back("Max-Age: 0");
                res.headers.push_back("Expires: 0");
                res.headers.push_back("Cache-Control: no-cache, private");
                res.headers.push_back("Pragma: no-cache");
                res.headers.push_back("Content-Type: multipart/x-mixed-replace;boundary=--boundary");
                if (!res.send_header())
                    return;

                std::vector<uchar> buf;

                while(true) {
                    rs2::frameset data = pipe.wait_for_frames(); // Wait for next set of frames from the camera
                    data = align_to_color.process( data );       // Replace with aligned frames
                    auto depth_frame = data.get_depth_frame();
                    auto color_frame = data.get_color_frame();

                    cv::Mat img(1, 1, CV_8UC4, cv::Scalar(0,0,0,0));

                    get_face(img, color_frame, depth_frame);

//                    mtx.lock();
//                    v >> m;
//                    mtx.unlock();
//                    std::cout << &m << std::endl;
                    cv::Mat dst;               // dst must be a different Mat
                    cv::flip(img, dst, 1);
                    cv::imencode(".png", dst, buf);
                    std::string image (buf.begin(), buf.end());
                    if (!res.send_msg("--boundary\r\n"
                                      "Content-Type: image/png\r\n"
                                      "Content-Length: " +
                                      std::to_string(image.size()) +
                                      "\r\n\r\n" +
                                      image))
                        break;
                }

                pipe.stop();
            }).get("/", [&] (Request req, Response res){
                res >> "<html>"
                       "    <body>"
                       "        <h1>CAMERA STREAMING</h1>"
                       /// Set the correct ip address
                       "        <img style=\"width: 480px; height: 480px;\" src='http://localhost:5555/video_feed'/>"
                       "    </body>"
                       "</html>";
            }).listen();
//
        }

        int start(){

            return 0;
//            return get_rs_frame();
        }

        bool is_prime () {
            get_rs_frame();
            return true;
        }

    private:
        float DEVICE_DEPTH_SCALE;
        rs2::pipeline pipe;
        rs2::pipeline_profile profile;

        http::Server* s;
        std::mutex mtx;

        dlib::frontal_face_detector face_bbox_detector = dlib::get_frontal_face_detector();
        dlib::shape_predictor face_landmark_annotator;
        anet_type net;

        bool compare_face(dlib::full_object_detection &face1, dlib::full_object_detection &face2){

//            if( operation == nullptr ){
//                std::cout << face.get_rect() << std::endl;
//                return false;
//            }
//            return operation(face, image);
        }

        float get_depth_scale( rs2::device dev )
        {
            // Go over the device's sensors
            for( rs2::sensor& sensor : dev.query_sensors() )
            {
                // Check if the sensor if a depth sensor
                if( rs2::depth_sensor dpt = sensor.as<rs2::depth_sensor>() )
                {
                    return dpt.get_depth_scale();
                }
            }
            throw std::runtime_error( "Device does not have a depth sensor" );
        }

        void get_face(cv::Mat &img, rs2::video_frame &color_frame, rs2::depth_frame &depth_frame){
            // Create a dlib image for face detection
            rs_frame_image< dlib::rgb_pixel, RS2_FORMAT_RGB8 > image( color_frame );

            // Detect faces: find bounding boxes around all faces, then annotate each to find its landmarks
            std::vector< dlib::rectangle > face_bboxes = face_bbox_detector( image );
            std::vector< dlib::full_object_detection > faces;
            float distance = 100;
            dlib::full_object_detection face;
            bool has_face = false;

            for( auto const & bbox : face_bboxes ) {
                auto left = bbox.left();
                auto right = bbox.right();
                auto top = bbox.top();
                auto bottom = bbox.bottom();
                const long depth_point[2] = {
                    (left + ((right - left) / 2)),
                    (top + ((bottom - top) / 2))
                };
                auto dist_from_center = depth_frame.get_distance((int)depth_point[0], (int)depth_point[1]);
                if( dist_from_center < distance){
                    distance = dist_from_center;
                    face = face_landmark_annotator(image, bbox);
                    has_face = true;
                }
//                faces.push_back(face_landmark_annotator(image, bbox));
            }

            if( has_face ){
                img = frame_to_mat(color_frame);
                dlib::rgb_pixel & color =
                        validate_face( depth_frame, DEVICE_DEPTH_SCALE, face )
                        ? good_color : bad_color;
                std::vector<dlib::matrix<dlib::rgb_pixel>> helper;
                dlib::matrix<dlib::rgb_pixel> face_chip;
                extract_image_chip(image, get_face_chip_details(face,150,0.25), face_chip);
                helper.push_back(std::move(face_chip));
                std::vector<dlib::matrix<float,0,1>> face_descriptors = net(helper);
//                std::cout << length(face_descriptors[0]) << std::endl;
                if( length(id_descriptor - face_descriptors[0]) < 0.6 ){
                    std::cout << "MIREK" << std::endl;
                }else{
                    std::cout << "UNKNOWN" << std::endl;
                }
//                if (length(face_descriptors[i]-face_descriptors[j]) < 0.6)
//                    edges.push_back(sample_pair(i,j));
                render_face( img, face, color );
                int offset = 25;
                int left = (int)face.get_rect().left() - offset > 0 ? (int)face.get_rect().left() - offset : 0;
                int top = (int)face.get_rect().top() - offset > 0 ? (int)face.get_rect().top() - offset : 0;
                int right = (int)face.get_rect().right() + offset < color_frame.get_width() ? (int)face.get_rect().right() + offset : color_frame.get_width()-1;
                int bottom = (int)face.get_rect().bottom() + offset < color_frame.get_height() ? (int)face.get_rect().bottom() + offset : color_frame.get_height()-1;
                img = img(cv::Rect(cv::Point(left, top),cv::Point(right, bottom)));
            }else{
//                return NULL;
            }
//            for( auto const & face : faces )
//            {
//                dlib::rgb_pixel & color =
//                        validate_face( depth_frame, DEVICE_DEPTH_SCALE, face )
//                        ? good_color : bad_color;
//                render_face( img, face, color );
//            }
        }

        static void render_face( cv::Mat &img,
                dlib::full_object_detection const & face,
                dlib::rgb_pixel const & color
        )
        {

            cv::Scalar col( color.blue, color.green, color.red );


            // Around Chin. Ear to Ear
            for( unsigned long i = markup_68::JAW_FROM; i < markup_68::JAW_TO; ++i )
                render_face_element(img, face, col, i, i+1);

            // Nose
            for( unsigned long i = markup_68::NOSE_RIDGE_FROM; i < markup_68::NOSE_RIDGE_TO; ++i )
                render_face_element(img, face, col, i, i+1);
            render_face_element(img, face, col, markup_68::NOSE_TIP, markup_68::NOSE_BOTTOM_FROM);
            render_face_element(img, face, col, markup_68::NOSE_TIP, markup_68::NOSE_BOTTOM_TO);

            // Left eyebrow
            for( unsigned long i = markup_68::RIGHT_EYEBROW_FROM; i < markup_68::RIGHT_EYEBROW_TO; ++i )
                render_face_element(img, face, col, i, i+1);

            // Right eyebrow
            for( unsigned long i = markup_68::LEFT_EYEBROW_FROM; i < markup_68::LEFT_EYEBROW_TO; ++i )
                render_face_element(img, face, col, i, i+1);

            // Right eye
            for( unsigned long i = markup_68::RIGHT_EYE_FROM; i < markup_68::RIGHT_EYE_TO; ++i )
                render_face_element(img, face, col, i, i+1);
            render_face_element(img, face, col, markup_68::RIGHT_EYE_FROM, markup_68::RIGHT_EYE_TO);

            // Left eye
            for( unsigned long i = markup_68::LEFT_EYE_FROM; i < markup_68::LEFT_EYE_TO; ++i )
                render_face_element(img, face, col, i, i+1);
            render_face_element(img, face, col, markup_68::LEFT_EYE_FROM, markup_68::LEFT_EYE_TO);

            // Lips inside part
            for( unsigned long i = markup_68::MOUTH_INNER_FROM; i < markup_68::MOUTH_INNER_TO; ++i )
                render_face_element(img, face, col, i, i+1);
            render_face_element(img, face, col, markup_68::MOUTH_INNER_FROM, markup_68::MOUTH_INNER_TO);

            // Setup a rectangle to define your region of interest
//            cv::Rect myROI(10, 10, 100, 100);

            // Crop the full image to that image contained by the rectangle myROI
            // Note that this doesn't copy the data
//            cv::Mat croppedImage = img(myROI);
//            img = img(cv::Rect(face.part(markup_68::JAW_FROM).x(),face.part(markup_68::JAW_FROM).y(),200,200));
//            img = croppedImage;
//            delete croppedImage;
        }

        int get_rs_frame() try
        {
//            data = align_to_color.process( data );       // Replace with aligned frames
//            auto depth_frame = data.get_depth_frame();
//            auto color_frame = data.get_color_frame();

            return EXIT_SUCCESS;
        }
        catch( dlib::serialization_error const & e )
        {
            std::cerr << "You need dlib's default face landmarking model file to run this example." << std::endl;
            std::cerr << "You can get it from the following URL: " << std::endl;
            std::cerr << "   http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2" << std::endl;
            std::cerr << std::endl << e.what() << std::endl;
            return EXIT_FAILURE;
        }
        catch (const rs2::error & e)
        {
            std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
            return EXIT_FAILURE;
        }
        catch (const std::exception& e)
        {
            std::cerr << e.what() << std::endl;
            return EXIT_FAILURE;
        }
};

//void MyClass::myMethod() {
//    std::cout << "Hello World!";
//}
#endif //REALSENSEDLIBSAMPLES_MYCLASS_H
