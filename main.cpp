#include <iostream>
//#include <opencv4/opencv2/core/mat.hpp>
#include <unistd.h>
#include "FaceRecognizer.h"
#include <thread>

fr::pipeline * face_recognizer;

bool stop = false;
static void loop(){
    std::cout << "start loop" << std::endl;
    while(!stop){
        fr::current_frame* frame_data = face_recognizer->wait_for_frame();
        std::cout << frame_data->recognition_status << std::endl;
        sleep(1);
    }
    std::cout << "loop ended" << std::endl;
}

int main(int arv, char** args) {
    face_recognizer = new fr::pipeline(1920,1080);
    cv::Mat id_image = cv::imread(args[1]);
    dlib::matrix<float,0,1> descr = face_recognizer->get_image_descriptor(fr::mat_to_rgbpixel(id_image));
    std::cout << descr.size() << std::endl;
//    return 0;
    face_recognizer->set_id_descriptor(&descr);
    face_recognizer->start_pipe();
    std::thread t(loop); // Separate thread for loop.
    std::cin.get();
    stop = true;
    sleep(1);
    t.join();
    face_recognizer->stop_pipe();
    delete face_recognizer;
    return 0;
}