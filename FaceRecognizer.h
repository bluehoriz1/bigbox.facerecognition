//
// Created by mireo on 09.05.2021.
//

#include <iostream>
#include <librealsense2/rs.hpp>   // Include RealSense Cross Platform API
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include "rs2/dlib/rs_frame_image.h"
#include "rs2/dlib/face/validate_face.h"
#include "rs2/opencv/cv-helpers.hpp"

#include "compare_helper.h"

namespace fr {

    const int NO_PERSON = 2;
    const int WRONG_PERSON = 0;
    const int CORRECT_PERSON = 1;
    const int NEVER_USED = -1;
    class pipeline;
    typedef void recognition_status(fr::pipeline* self, int status);

    struct current_frame {
        cv::Mat * cv_frame = new cv::Mat(1, 1, CV_8UC4, cv::Scalar(0, 0, 0, 0));
        int frame_number = 0;
        int recognition_status = fr::NEVER_USED;
        bool is_reading = false;
    };

    class pipeline {

    private:

        rs2::config cfg;
        rs2::pipeline * pipe;

        dlib::rgb_pixel bad_color = {255, 0, 0};
        dlib::rgb_pixel person_color = {0, 0, 255};
        dlib::rgb_pixel good_color = {0, 255, 0};

        dlib::matrix<float, 0, 1> * id_descriptor = nullptr;

        anet_type net;

        dlib::frontal_face_detector face_bbox_detector =  dlib::get_frontal_face_detector();
        dlib::shape_predictor face_landmark_annotator;

    public:
        struct fr::current_frame current_frame;

        recognition_status * rs = nullptr;

        pipeline(int width = 640, int height = 480, int framerate = 15, rs2::pipeline * pipeline = new rs2::pipeline) {
            dlib::deserialize("shape_predictor_68_face_landmarks.dat") >> face_landmark_annotator;
            dlib::deserialize("dlib_face_recognition_resnet_model_v1.dat") >> net;
            face_bbox_detector = dlib::get_frontal_face_detector();
            cfg.enable_stream(RS2_STREAM_COLOR, width, height, RS2_FORMAT_RGB8, framerate);
            cfg.enable_stream(RS2_STREAM_DEPTH, width>1280?1280:width, height>720?720:height, RS2_FORMAT_Z16, framerate);

            pipe = pipeline;
        }

        ~pipeline() {
            delete current_frame.cv_frame;
            if ( pipe != nullptr )
                delete pipe;
//            std::cout << "after 1" << std::endl;
//            std::cout << id_descriptor->size() << std::endl;
//            if( id_descriptor != nullptr )
//                delete id_descriptor;
//            std::cout << "after 2" << std::endl;
        }

        rs2::align align_to_color = rs2::align( RS2_STREAM_COLOR );
        rs2::pipeline_profile profile;
        float device_depth_scale;

        void start_pipe(){
            current_frame = fr::current_frame();
            current_frame.frame_number = 0;
            current_frame.recognition_status = fr::NEVER_USED;
            current_frame.is_reading = true;
            current_frame.cv_frame = new cv::Mat(1, 1, CV_8UC4, cv::Scalar(0, 0, 0, 0));
//            align_to_color = new( RS2_STREAM_COLOR );
            profile = pipe->start(cfg);
            device_depth_scale = get_depth_scale(profile.get_device());
        }

        void stop_pipe(){
            rs = nullptr;
            current_frame.is_reading = false;
            pipe->stop();
        }

        fr::current_frame * wait_for_frame(){
            int current_status_value = fr::NEVER_USED;
            rs2::frameset data = pipe->wait_for_frames();

            data = align_to_color.process(data);       // Replace with aligned frames
            auto depth_frame = data.get_depth_frame();
            auto color_frame = data.get_color_frame();
            cv::Mat img(1, 1, CV_8UC4, cv::Scalar(0, 0, 0, 0));
            if( id_descriptor != nullptr ) {
                // Create a dlib image for face detection
                rs_frame_image<dlib::rgb_pixel, RS2_FORMAT_RGB8> dlib_image(color_frame);
                dlib::full_object_detection face_shape = select_face(dlib_image, depth_frame);
                if (face_shape.num_parts() != 0 && face_shape.num_parts() == 68) {
                    bool anti_spoofing = validate_face(depth_frame, device_depth_scale, face_shape);
//                    bool anti_spoofing = true;
                    dlib::matrix<dlib::rgb_pixel> face_chip;
                    extract_image_chip(dlib_image, get_face_chip_details(face_shape, 150, 0.25), face_chip);
                    dlib::matrix<float, 0, 1> face_descriptor = net(face_chip);
                    if (length(*id_descriptor - face_descriptor) < 0.6) {
                        if (anti_spoofing) {
                            current_status_value = fr::CORRECT_PERSON;
                        }else{
                            current_status_value = fr::NO_PERSON;
                        }
                    } else {
                        current_status_value = fr::WRONG_PERSON;
                    }
                    dlib::rgb_pixel &color =
                            current_status_value != fr::WRONG_PERSON ? (current_status_value == fr::CORRECT_PERSON
                                                                        ? good_color : person_color) : bad_color;
                    img = fr::render_face_view(face_shape, color_frame, color, color_frame.get_width()/640*25);
                }

            }else{
                img = frame_to_mat(color_frame);
            }
            if(!current_frame.is_reading) {
                return &current_frame;
            }
            if( rs != nullptr ){
//                if( current_status_value )
                    rs(this, current_status_value);
            }
            if( id_descriptor == nullptr || current_status_value != fr::NEVER_USED )
                *current_frame.cv_frame = img;
            current_frame.recognition_status = current_status_value;
            current_frame.frame_number++;
            return &current_frame;
        }

        void set_id_descriptor(dlib::matrix<float, 0, 1> * id_desc) {
            id_descriptor = id_desc;
        }

        void clear_id_descriptor(){
            if( id_descriptor != nullptr )
                delete id_descriptor;
            id_descriptor = nullptr;
        }

        dlib::full_object_detection select_face(const rs_frame_image<dlib::rgb_pixel, RS2_FORMAT_RGB8> &image, const rs2::depth_frame &depth_frame) {

            // Detect faces: find bounding boxes around all faces, then annotate each to find its landmarks
            std::vector<dlib::rectangle> face_bboxes = face_bbox_detector(image);
            std::vector<dlib::full_object_detection> faces;
            float distance = 100;
            dlib::full_object_detection face;
            for (auto const &bbox : face_bboxes) {
                auto left = bbox.left();
                auto right = bbox.right();
                auto top = bbox.top();
                auto bottom = bbox.bottom();
                const long depth_point[2] = {
                        (left + ((right - left) / 2)),
                        (top + ((bottom - top) / 2))
                };
                auto dist_from_center = depth_frame.get_distance((int) depth_point[0], (int) depth_point[1]);
                if (dist_from_center < distance) {
                    distance = dist_from_center;
                    face = face_landmark_annotator(image, bbox);
                }
            }

            return face;
        }

        dlib::matrix<float, 0, 1> get_image_descriptor(dlib::matrix<dlib::rgb_pixel> const &image) {
            std::vector<dlib::rectangle> face_bboxes = face_bbox_detector(image);
            std::vector<dlib::full_object_detection> faces;
            dlib::matrix<float, 0, 1> id_descr;
            if (!face_bboxes.empty()) {
                dlib::full_object_detection shape = face_landmark_annotator(image, face_bboxes[0]);
                id_descr = get_face_descriptor(image, shape);
            }
            return id_descr;
        }

        dlib::matrix<float, 0, 1>
            get_face_descriptor(const dlib::matrix<dlib::rgb_pixel> &image, const dlib::full_object_detection &shape) {
            return net(get_face_chip(image, shape));
        }
    };
}
