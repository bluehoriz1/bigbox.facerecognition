//
// Created by mireo on 03.05.2021.
//

#include <iostream>
#include "FaceRecognizer.h"
//#include "compare_helper.h"


#ifdef __cplusplus    // If used by C++ code,
extern "C" {          // we need to export the C interface
#endif

//    typedef struct Temp
//    {       int * iVal;
//        char * chVal;
//    }MyTemp;

    /*___________________PLATFORM BASED EXPORT DECLARATION STARTS_____*/
    #ifdef __linux__
    #define EXPORT extern
    #elif (defined (_WIN32))
    #define EXPORT extern __declspec( dllexport )
    #endif
    /*___________________PLATFORM BASED EXPORT DECLARATION ENDS________*/

    EXPORT fr::pipeline * create_fr(int width = 640, int height = 480, int framerate = 15){
//    EXPORT fr::pipeline * create_fr(){
        return new fr::pipeline(width, height, framerate);
        return new fr::pipeline();
    }

    EXPORT void start_pipe_fr(fr::pipeline * fr){
        fr->start_pipe();
    }

    EXPORT void stop_pipe_fr(fr::pipeline * fr){
        fr->stop_pipe();
    }

    EXPORT void wait_for_frame_fr(fr::pipeline * fr, cv::Mat * mat){
        if( fr->current_frame.is_reading ){
            try{
                *mat = *(fr->wait_for_frame())->cv_frame;
            }catch(std::exception ex){
                std::cerr << ex.what() << std::endl;
                *mat = *(fr->current_frame).cv_frame;
            }
        }else{
            *mat = *(fr->current_frame).cv_frame;
        }
    }

    EXPORT void status_face_recognizer_fr(fr::pipeline * fr, fr::recognition_status rs){
        fr->rs = rs;
    }

    EXPORT cv::Mat * get_current_frame_fr(fr::pipeline * fr){
        return fr->current_frame.cv_frame;
    }

    EXPORT int get_current_recogniction_status_fr(fr::pipeline * fr){
        return fr->current_frame.recognition_status;
    }

    EXPORT void set_id_descriptor_fr(fr::pipeline * fr, dlib::matrix<float,0,1> * id_descriptor){
        if( id_descriptor->size() > 0 ) {
            fr->set_id_descriptor(new dlib::matrix<float,0,1>(*id_descriptor));
        }else{
            std::cout << "No face data" << std::endl;
        }
    }

    EXPORT void clear_id_descriptor_fr(fr::pipeline * fr){
        fr->clear_id_descriptor();
    }

    EXPORT dlib::matrix<float,0,1>* get_image_descriptor_from_mat_fr( fr::pipeline * fr, cv::Mat* image, bool * isFace ){
        dlib::matrix<float,0,1> descr = fr->get_image_descriptor(fr::mat_to_rgbpixel(*image));
        delete image;
        *isFace = false;
        if( descr.size() > 0 ){
            *isFace = true;
        }
        return new dlib::matrix<float,0,1>(descr);
    }

    EXPORT void destroy_fr( fr::pipeline * object )
    {
        delete object;
    }

#ifdef __cplusplus
}
#endif
