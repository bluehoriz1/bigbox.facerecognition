# BigBox.FaceRecognition C++ library

BigBox.FaceRecognition is a modern C++ toolkit containing machine learning algorithms and tools in C++ to solve face recognition dilema.

## Compiling BigBox.FaceRecognition C++ example program

```bash
mkdir build; cd build; cmake .. ; cmake --build .
```

That will build all the examples.

## BigBox.FaceRecognition shared library

The BigBox.FaceRecognition library builds shared library to futher use as wrapper class in C#,.NET or any other programming solution 

This library is licensed under the MIT License. The long and short of the license is that you can use BigBox.FaceRecognition however you like, even in closed source commercial software.

## BigBox.FaceRecognition sponsors

This research is based in part upon work supported from the EU funds by BlueHorizone Siemianowice Śląskie

Blue Horizone Sp. z o.o. realizuje projekt dofinansowany z Funduszy Europejskich pt. „ Opracowanie nowego wzoru przemysłowego samoobsługowego urządzenia vendingowego zdolnego do weryfikacji tożsamości konsumenta”
Okres realizacji projektu: 2020-01-01 – 2021-04-30